/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __UBLOX7_H
#define __UBLOX7_H

#ifdef __cplusplus
extern "C" {
#endif


#include "main.h"
#include "usart.h"

#define GPS_UART &huart1 //remember to add callback function in xxx_it.c

uint16_t GPS_uart_rx_len;
#define GPS_uart_buf_size 300
uint8_t GPS_uart_buf[GPS_uart_buf_size];

#define NAV_PVT_ID  0x0107


uint8_t SetDefaultSetting[] = 	{0xB5,0x62,0x06,0x09,
								0x0D,0x00,0xFF,0xFB,
								0x00,0x00,0x00,0x00,
								0x00,0x00,0xFF,0xFF,
								0x00,0x00,0x17,0x2B,
								0x7E};

uint8_t SetUart1[] = 	{0xB5,0x62,0x06,0x00,
						0x14,0x00,0x01,0x00,
						0x00,0x00,0xD0,0x08,
						0x00,0x00,0x00,0xC2,
						0x01,0x00,0x01,0x00,
						0x01,0x00,0x00,0x00,
						0x00,0x00,0xB8,0x42};//115200

uint8_t SetNone[] = 	{0xB5,0x62,0x06,0x00,
						0x14,0x00,0x01,0x00,
						0x00,0x00,0xD0,0x08,
						0x00,0x00,0x80,0x25,
						0x00,0x00,0x00,0x00,
						0x00,0x00,0x00,0x00,
						0x00,0x00,0x98,0x6B};//9600

uint8_t SetRate[] = 	{0xB5,0x62,0x06,0x08,
						0x06,0x00,0x64,0x00,//
						0x01,0x00,0x01,0x00,
						0x7A,0x12};//
// 1000ms :0x03,0xE8   0xDE,0x6A  //100ms  0x64,0x00   0x7A,0x12

uint8_t SetNAV_PVT[] = 	{0xB5,0x62,0x06,0x01,
						0x08,0x00,0x01,0x07,
						0x00,0x01,0x00,0x00, //change the second byte to 0 turn it off
						0x00,0x00,0x18,0xE1};


typedef struct GPS_DATA{
	
	int mSec;
	int Year;
	int Month;
	int Day;
	int Hour;
	int Min;
	int Sec;
	
	int Longitude;
	int Latitude;
	
	int Speed;
	int Heading;
	int SpeedAcc;
	int HeadingAcc;

} GPSData;

GPSData GPS_Data;
// mSec; Year; Month; Day; Hour; Min; Sec; Longitude; Latitude; Speed; Heading; SpeedAcc; HeadingAcc;
uint8_t StartByte[] = {0,4,6,7,8,9,10,24,28,60,64,68,72};
uint8_t    Length[] = {4,2,1,1,1,1, 1, 4, 4, 4, 4, 4, 4};

uint8_t GPS_Data_Avaliable =0;

typedef enum {
	GPS_OFF,
	GPS_Initilized,
	GPS_Ready,
	GPS_OK,
	GPS_SetUpError,
	GPS_NoSignal,
	GPS_DataInvalid,
} GPS_STATUS;

GPS_STATUS GPSstatus;
uint8_t GotSignal=0;
uint8_t GotData = 0;
uint8_t GPS_FailCount=0;

void GPS_HardReset(void);
void GPS_Configure(void);

void GPS_Start_UART(void);

void GPS_ProcessPVTData(uint8_t* src, GPSData* dst);

//void GPS_UART_IDLE_Callback(UART_HandleTypeDef *huart);

//void GPS_GotSignal(void);

//void GPS_TimerCallback(void);

//void GPS_Diagnostic(void);

void GPS_HardReset(void){
	HAL_GPIO_WritePin(GPS_RESET_GPIO_Port, GPS_RESET_Pin, GPIO_PIN_RESET);
	HAL_Delay(10);
	HAL_GPIO_WritePin(GPS_RESET_GPIO_Port, GPS_RESET_Pin, GPIO_PIN_SET);

}

void GPS_Configure(void) {

	GPSstatus=GPS_OFF;

	GPS_HardReset();

	HAL_Delay(200);

	if(HAL_UART_Transmit(GPS_UART, &SetDefaultSetting[0], sizeof(SetDefaultSetting), 1000)!=HAL_OK){
		GPSstatus = GPS_SetUpError;
		return;
	}

	HAL_Delay(30);
	//set uart1 on 115200 and the other channles off
	if(HAL_UART_Transmit(GPS_UART, &SetUart1[0], sizeof(SetUart1), 1000)!=HAL_OK){
		GPSstatus = GPS_SetUpError;
		return;
	}
	HAL_Delay(30);

	*GPS_UART.Init.BaudRate=115200;
	if(HAL_UART_Init(GPS_UART)!=HAL_OK){
		GPSstatus = GPS_SetUpError;
		return;
	}
	if(HAL_UART_Transmit(GPS_UART, &SetNAV_PVT[0], sizeof(SetNAV_PVT), 1000)!=HAL_OK){
		GPSstatus = GPS_SetUpError;
		return;
	}
	HAL_Delay(30);

	//set the output rate
	if(HAL_UART_Transmit(GPS_UART, &SetRate[0], sizeof(SetRate), 1000)!=HAL_OK){
		GPSstatus = GPS_SetUpError;
		return;
	}
	HAL_Delay(30);
	GPSstatus = GPS_Initilized;

	GotSignal = 0;
	HAL_TIM_Base_Start_IT(&htim4); //timer to check signal

	GPS_Data_Avaliable=0;
	GPS_Start_UART(); //start receiving gps data
}

void GPS_Diagnostic(void){
	if(GPS_FailCount==5){

	}
	else if((GPSstatus==GPS_SetUpError)||(GPSstatus==GPS_DataInvalid)){
		GPS_Configure();
		GPS_FailCount ++;
	}
}

void GPS_Start_UART(void)
{

	__HAL_UART_CLEAR_IDLEFLAG(GPS_UART);
	__HAL_UART_ENABLE_IT(GPS_UART, UART_IT_IDLE);

	HAL_UART_Receive_DMA(GPS_UART, GPS_uart_buf,GPS_uart_buf_size);
	CLEAR_BIT((&huart1)->Instance->CR3, USART_CR3_EIE);

}

void GPS_GotSignal(void){
	GotSignal = 1;
}

void GPS_TimerCallback(void){
	if(GotSignal){
		if(GPSstatus == GPS_Initilized||GPSstatus == GPS_NoSignal){
			GotData = 0;
			GPSstatus = GPS_Ready;
		}
		GotSignal = 0;
	}else{
		GPSstatus = GPS_NoSignal;
	}
	if(GPSstatus==GPS_Ready||GPSstatus == GPS_OK){
		if(GotData==1){
			GPSstatus = GPS_OK;
			GotData = 0;
		}else{
			if(GotData==-2){
				GPSstatus = GPS_DataInvalid;
			}
			GotData--;
		}
	}
}

void GPS_UART_IDLE_Callback(UART_HandleTypeDef *huart)
{

		if(huart->Instance==USART1){
			uint32_t tmp1 = __HAL_UART_GET_FLAG(huart, UART_FLAG_IDLE);
			uint32_t tmp2 = __HAL_UART_GET_IT_SOURCE(huart, UART_IT_IDLE);

			if((tmp1 != RESET) && (tmp2 == 16))
			{
				/*Stop the DMA*/
				HAL_UART_DMAStop(huart);
				__HAL_UART_CLEAR_IDLEFLAG(huart);

				/* get rx data len */
				GPS_uart_rx_len = GPS_uart_buf_size - huart->hdmarx->Instance->CNDTR;

				/* restart the dma*/
				__HAL_UART_CLEAR_IDLEFLAG(huart);
				HAL_UART_Receive_DMA(huart,GPS_uart_buf,(uint16_t)GPS_uart_buf_size);
				CLEAR_BIT((&huart1)->Instance->CR3, USART_CR3_EIE);
				/*check data is good using ublox check sum*/
				uint8_t CK_A = 0, CK_B = 0;
				for(uint16_t I=2;I<GPS_uart_rx_len-2;I++)
				{
				CK_A = CK_A + GPS_uart_buf[I];
				CK_B = CK_B + CK_A;
				}
				if(CK_A == GPS_uart_buf[GPS_uart_rx_len-2]&& CK_B == GPS_uart_buf[GPS_uart_rx_len-1]){
					uint16_t ID = (GPS_uart_buf[2]<<8)+GPS_uart_buf[3];
					if(ID==NAV_PVT_ID){
						GPS_ProcessPVTData(&GPS_uart_buf[0],&GPS_Data);
						GotData=1;
						GPS_Data_Avaliable = 1;
					}
				}
			}
		}

}


void GPS_ProcessPVTData(uint8_t* src, GPSData* dst){
	int* item;
	item = (int*)dst;

	for(uint8_t j = 0; j < 13; j++){
		int num = 0;
		for(uint8_t i = 0; i< Length[j]; i++){
			num += *(src+StartByte[j]+6+i)<<(8*i);  //trans hex to dec
		}
		*item = (int)num;
		item++;
	}

    dst->Speed = dst->Speed/10; //0.01m/s
	dst->Heading = dst->Heading/1000; //0.01 deg
	//dst->Longitude = dst->Longitude/10000000;
	//dst->Latitude = dst->Latitude/10000000;
	dst->Hour = dst->Hour+11;
	if(dst->Hour>=24) dst->Hour -= 24; //24 format
	dst->mSec = (int)dst->mSec%1000;

}


#ifdef __cplusplus
}
#endif

#endif 

#ifndef __MEMS__H
#define __MEMS__H
#include "main.h"

#ifndef __ARCHDEP__TYPES
#define __ARCHDEP__TYPES

typedef unsigned char u8_t;
typedef unsigned short int u16_t;
typedef unsigned int u32_t;
typedef int i32_t;
typedef short int i16_t;
typedef signed char i8_t;

#endif /*__ARCHDEP__TYPES*/


typedef enum
{
  MEMS_SUCCESS      =   0x01,
  MEMS_ERROR        =   0x00
} status_t;

typedef enum{
	MEMS_SENSOR_ERROR,
	MEMS_SENSOR_OFF,
	MEMS_SENSOR_ON
}MEMS_STATUS;

typedef enum{
	LIS3DSH = 0x3F,
	LSM6DS3 = 0x69,
	NO_SENSOR = 0
}MEMS_TYPE;

typedef struct{
	MEMS_TYPE SensorType;
	MEMS_STATUS SensorStatus;
}MEMS_SENSOR;


typedef enum {
  MEMS_ENABLE				=		0x01,
  MEMS_DISABLE				=		0x00
} State_t;

typedef struct {
  i16_t AXIS_X;
  i16_t AXIS_Y;
  i16_t AXIS_Z;
} AxesRaw_t;


extern SPI_HandleTypeDef hspi1;  //could change to have handle be this
status_t MEMS_initGPIO(void);
status_t MEMS_initSPI(SPI_HandleTypeDef *hspi);
uint8_t MEMS_Read_Reg(void *handle, uint8_t ReadAddr,  uint8_t *pBuffer, uint16_t nBytesToRead);
uint8_t MEMS_Write_Reg(void *handle, uint8_t WriteAddr, uint8_t *pBuffer, uint16_t nBytesToWrite);

//#define MEMS_CS_Pin 							GPIO_PIN_4
//#define MEMS_CS_GPIO_Port 						GPIOA
#define __MEMS_CS_GPIO_Port_CLK_ENABLE			__HAL_RCC_GPIOA_CLK_ENABLE()
#define __MEMS_CS_LOW  							HAL_GPIO_WritePin(MEMS_CS_GPIO_Port,MEMS_CS_Pin, GPIO_PIN_RESET)
#define __MEMS_CS_HIGH  						HAL_GPIO_WritePin(MEMS_CS_GPIO_Port,MEMS_CS_Pin, GPIO_PIN_SET)

#define SPI_HANDLE &hspi1
#define LSM6DS3_CS_Pin 				GPIO_PIN_4
#define LSM6DS3_CS_GPIO_Port 		GPIOA
#define __LSM6DS3_CS_GPIO_Port_CLK_ENABLE	__HAL_RCC_GPIOA_CLK_ENABLE()
#define __LSM6DS3_CS_LOW  	HAL_GPIO_WritePin(LSM6DS3_CS_GPIO_Port,LSM6DS3_CS_Pin, GPIO_PIN_RESET)
#define __LSM6DS3_CS_HIGH  	HAL_GPIO_WritePin(LSM6DS3_CS_GPIO_Port,LSM6DS3_CS_Pin, GPIO_PIN_SET)


#endif

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MEMS_LSM6DS3_H
#define __MEMS_LSM6DS3_H

#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"
#include "MEMS.h"
#include "LSM6DS3_ACC_GYRO_driver.h"
#include "arm_math.h"

MEMS_SENSOR MemsActiveSensor;

typedef enum {
	MEMS_OFF,
	MEMS_OK,
	MEMS_Error,
} MEMSSTATUS;

MEMSSTATUS MEMSstatus=MEMS_OFF;

MEMSSTATUS MEMS_Init(void);
MEMSSTATUS MEMS_Cal(void);
uint8_t MEMS_FailCount = 0;
void MEMS_Diagnostic(void);
void MEMS_FilterInit(void);
status_t 	LSM6DS3_Init(void);

status_t 	LSM6DS3_SetInterrupt(void);
status_t LSM6DS3_SetInterruptOff(void);


#define MEMS_SPI &hspi1

uint8_t MEMS_Data_Avaliable=0;
float32_t MEMS_YawRawData=0;
float32_t MEMS_NullOutput = 0;
float32_t MEMS_YawRate = 0;
int MEMS_RawData[9];
//int xlData[3];
//int DataToSerial[7];
#define TEST_LENGTH_SAMPLES   3
#define NUM_STAGES 			  1

static float32_t iirStateF32[2*NUM_STAGES];
const float32_t iirCoeffs32[5*NUM_STAGES] = {
		6.555981325091302e-04,0.001311196265018,6.555981325091302e-04,1.926279786172059,-0.928902178702096  // Negate the original a1 a2 from Matlab
};

//0.002080567135492,0.004161134270985,0.002080567135492,1.866892279711715,-0.875214548253684 //1.5 kind of no delay
//6.555981325091302e-04,0.001311196265018,6.555981325091302e-04,1.926279786172059,-0.928902178702096//0.83 better delay
//1.027603819981147e-04,2.055207639962294e-04,1.027603819981147e-04,1.971123225039501,-0.971534266567493  //0.325 bes match with sample yaw rate calculated by spped from GPS, but a bad delay

float32_t  IIRInput[TEST_LENGTH_SAMPLES], IIROutput[TEST_LENGTH_SAMPLES];
arm_biquad_cascade_df2T_instance_f32 S;
uint16_t IIR_counter = 0;
uint16_t IIR_ready = 0;
float32_t  MEMS_IIRYaw;

MEMSSTATUS MEMS_Init(void) {
	uint8_t response =0;
	MEMSstatus = MEMS_OFF;
	/*power cycle the sensor*/

	//HAL_SPI_Transmit(&hspi1,0,1,100);
	//HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_SET);
	//HAL_GPIO_WritePin(MEMS_CS_GPIO_Port,MEMS_CS_Pin,GPIO_PIN_SET);

	HAL_GPIO_WritePin(MEMS_PWR_GPIO_Port, MEMS_PWR_Pin, GPIO_PIN_RESET);
	HAL_Delay(10);
	HAL_GPIO_WritePin(MEMS_PWR_GPIO_Port,MEMS_PWR_Pin, GPIO_PIN_SET);
	HAL_Delay(10);


	LSM6DS3_ACC_GYRO_R_WHO_AM_I(MEMS_SPI, &response);
	HAL_Delay(20);

	if (response == LSM6DS3_ACC_GYRO_WHO_AM_I) {
		MemsActiveSensor.SensorType = LSM6DS3;
		if (LSM6DS3_Init() == MEMS_SUCCESS) {
			//LSM6DS3_Gyro_Cal();
			LSM6DS3_SetInterrupt();
			MEMSstatus = MEMS_OK;
		}
		else {
			MEMSstatus = MEMS_Error;
		}
	}
	if(MEMSstatus == MEMS_OK){
		HAL_Delay(10);
		MEMS_FilterInit();
		MEMSstatus = MEMS_Cal();
	}

	return MEMSstatus;
}

void MEMS_Diagnostic(void){
	if(MEMS_FailCount==5){

	}else{
		MEMS_Init();
		MEMS_FailCount++;
	}
}

status_t LSM6DS3_Init(void) {
	/*
	* if using UART to send data need to run ODR at 104 or maybe 208
	*/
	uint8_t result=0;

	//Test have the correct sensor, if no bail.
	if (MemsActiveSensor.SensorType != LSM6DS3) return MEMS_ERROR;

	/**** Setup MEMS, but not interupts ****/
	//reset the device, no issue here, hard reset be better (power cycle)
	LSM6DS3_ACC_GYRO_W_SW_RESET(MEMS_SPI, LSM6DS3_ACC_GYRO_SW_RESET_RESET_DEVICE);

	//on power change dealy data ready until filter has settled, not needed this appication but leave.
	//LSM6DS3_ACC_GYRO_W_DRDY_MSK(MEMS_SPI, LSM6DS3_ACC_GYRO_DRDY_MSK_ENABLED);

	//needed only if read is slower than the MEMS ODR, use anyway as will not effect this application.
	LSM6DS3_ACC_GYRO_W_BDU(MEMS_SPI, LSM6DS3_ACC_GYRO_BDU_BLOCK_UPDATE);

	/**** Setup Gyro ****/
	//enable Gyro, x,y,z, not this one only use the Z, but leave the others for interest until production
	 LSM6DS3_ACC_GYRO_W_ZEN_G(MEMS_SPI, LSM6DS3_ACC_GYRO_ZEN_G_ENABLED);
	LSM6DS3_ACC_GYRO_W_YEN_G(MEMS_SPI, LSM6DS3_ACC_GYRO_YEN_G_ENABLED);
	LSM6DS3_ACC_GYRO_W_XEN_G(MEMS_SPI, LSM6DS3_ACC_GYRO_XEN_G_ENABLED);

	 LSM6DS3_ACC_GYRO_W_ZEN_XL(MEMS_SPI, LSM6DS3_ACC_GYRO_ZEN_XL_ENABLED);
	LSM6DS3_ACC_GYRO_W_YEN_XL(MEMS_SPI, LSM6DS3_ACC_GYRO_YEN_XL_ENABLED);
	LSM6DS3_ACC_GYRO_W_XEN_XL(MEMS_SPI, LSM6DS3_ACC_GYRO_XEN_XL_ENABLED);

	//turn off high pass filter
	LSM6DS3_ACC_GYRO_W_HPFilter_En(MEMS_SPI, LSM6DS3_ACC_GYRO_HP_EN_DISABLED);

	//check the filter setting...
	LSM6DS3_ACC_GYRO_ReadReg(MEMS_SPI, LSM6DS3_ACC_GYRO_CTRL7_G, &result, 1);

	//set the sensitivity
	LSM6DS3_ACC_GYRO_W_FS_G(MEMS_SPI, LSM6DS3_ACC_GYRO_FS_G_1000dps); //245 500 1000
	LSM6DS3_ACC_GYRO_W_FS_XL(MEMS_SPI,LSM6DS3_ACC_GYRO_FS_XL_16g);
	//set the output data rate
	 LSM6DS3_ACC_GYRO_W_ODR_G(MEMS_SPI, LSM6DS3_ACC_GYRO_ODR_G_416Hz);//LSM6DS3_ACC_GYRO_ODR_XL_833Hz);//LSM6DS3_ACC_GYRO_ODR_XL_208Hz); //LSM6DS3_ACC_GYRO_ODR_XL_104Hz); //LSM6DS3_ACC_GYRO_ODR_G_416Hz);
	 LSM6DS3_ACC_GYRO_W_ODR_XL(MEMS_SPI, LSM6DS3_ACC_GYRO_ODR_XL_416Hz);

	//set the HP cut off frequency (if using it)
	//LSM6DS3_ACC_GYRO_W_HPCF_G(MEMS_SPI, LSM6DS3_ACC_GYRO_HPCF_G_2Hz07); //LSM6DS3_ACC_GYRO_HPCF_G_0Hz0324);  LSM6DS3_ACC_GYRO_HPCF_G_2Hz07);

//READ SOME REGS


#ifdef checkOK
																	  //read the register to see if set ok, could be done one by one...
	uint8_t valueRead;
	LSM6DS3_ACC_GYRO_ReadReg(MEMS_SPI, LSM6DS3_ACC_GYRO_CTRL1_XL, &valueRead, 1);		//for debug
	LSM6DS3_ACC_GYRO_ReadReg(MEMS_SPI, LSM6DS3_ACC_GYRO_CTRL9_XL, &valueRead, 1);		//for debug 0x38
	LSM6DS3_ACC_GYRO_ReadReg(MEMS_SPI, LSM6DS3_ACC_GYRO_CTRL10_C, &valueRead, 1);		//for debug
#endif

																					//should check the result before
																					//care read to that written
	return MEMS_SUCCESS;
}



status_t LSM6DS3_SetInterrupt(void) {
	//set interrupts
	status_t s = 0;
	MemsActiveSensor.SensorStatus = MEMS_SENSOR_ON;

	//set the INT1 singal level
	s = LSM6DS3_ACC_GYRO_W_INT_ACT_LEVEL(MEMS_SPI, LSM6DS3_ACC_GYRO_INT_ACT_LEVEL_ACTIVE_LO);
	s = LSM6DS3_ACC_GYRO_W_DRDY_XL_on_INT1(MEMS_SPI, LSM6DS3_ACC_GYRO_INT1_DRDY_XL_ENABLED);
	s = LSM6DS3_ACC_GYRO_W_DRDY_G_on_INT1(MEMS_SPI, LSM6DS3_ACC_GYRO_INT1_DRDY_G_ENABLED);
	return s;
}

status_t LSM6DS3_SetInterruptOff(void) {
	//set interrupts
	status_t s = 0;
	s = LSM6DS3_ACC_GYRO_W_DRDY_XL_on_INT1(MEMS_SPI, LSM6DS3_ACC_GYRO_INT1_DRDY_XL_DISABLED);
	s = LSM6DS3_ACC_GYRO_W_DRDY_G_on_INT1(MEMS_SPI, LSM6DS3_ACC_GYRO_INT1_DRDY_G_DISABLED);
	return s;
}

//assumption is that data is ready, this is called from the data ready interupt

MEMSSTATUS MEMS_Cal(void){
	HAL_Delay(50);
	uint16_t i = 0;
	float32_t Sum = 0;
	uint32_t start = HAL_GetTick();
   	while(i<512){
		if(MEMS_Data_Avaliable){
			Sum += MEMS_YawRawData;
			i+=1;
			MEMS_Data_Avaliable = 0;
		}
		if(HAL_GetTick()-start>5000){
			return MEMS_Error;
		}
	}
   	MEMS_NullOutput = Sum/(float32_t)512;
   	MEMS_RawData[0] = 16705;//00AA
   	MEMS_RawData[7] = (int)MEMS_NullOutput;
   	MEMS_RawData[8] = 1094778880;//AA00
	return MEMS_OK;
}

void MEMS_ProcessData(void){
	  LSM6DS3_ACC_Get_AngularRate(&hspi1, &MEMS_RawData[1], 0);  //read data from register
	  LSM6DS3_ACC_Get_Acceleration(&hspi1, &MEMS_RawData[4], 0);
	  MEMS_YawRawData = (float32_t)MEMS_RawData[3];
	  MEMS_YawRate = (MEMS_YawRawData - MEMS_NullOutput)*(1/(float32_t)1000);  //1000dps
	  if(MEMS_NullOutput!=0){
		  if(IIR_counter == TEST_LENGTH_SAMPLES){
			  arm_biquad_cascade_df2T_f32(&S, (float32_t*)&IIRInput[0], (float32_t*)&IIROutput[0], TEST_LENGTH_SAMPLES);
			  IIR_counter = 0;
			  IIR_ready = 1;
		  }
		  IIRInput[IIR_counter] = MEMS_YawRate;
		  if(IIR_ready){
			  MEMS_IIRYaw = IIROutput[IIR_counter];

		  }
		  IIR_counter += 1;
	  }
	  //HAL_UART_Transmit(&huart2,(uint8_t*)&MEMS_RawData[0],36,1000);
	  MEMS_Data_Avaliable = 1;
}

void MEMS_FilterInit(void){
	  arm_biquad_cascade_df2T_init_f32(&S, NUM_STAGES, (float32_t*)&iirCoeffs32[0],  (float32_t*)&iirStateF32[0]);
}



#ifdef __cplusplus
}
#endif

#endif 

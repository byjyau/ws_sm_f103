#include "MEMS.h"

/*******************************************************************************
* Function Name  : MEMS_initGPIO
* Description    : Set GPIO for comms to MEMS
* Input          : None
* Output         : None
* Return         : Status [MEMS_ERROR, MEMS_SUCCESS]
*******************************************************************************/
status_t MEMS_initGPIO(void){
    return MEMS_SUCCESS;
}



/*******************************************************************************
* Function Name  : MEMS_initSPI
* Description    : Set the SPI interface using HAL
* Input          : Handle to the SPI
* Output         : None
* Return         : Status [MEMS_ERROR, MEMS_SUCCESS]
*******************************************************************************/
status_t MEMS_initSPI(SPI_HandleTypeDef *hspi)
{
	return MEMS_SUCCESS;
}



/*******************************************************************************
* Function Name		: LIS3DH_ReadReg
* Description		: Generic Reading function. It must be fullfilled with either
*			: I2C or SPI reading functions
* Input			: Register Address
* Output		: Data REad
* Return		: None
*******************************************************************************/
uint8_t MEMS_Read_Reg(void *handle, uint8_t ReadAddr,  uint8_t *pBuffer, uint16_t nBytesToRead){ // u8_t Reg, u8_t* Data) {

	ReadAddr |= 0x80;
	uint8_t Tx[2] = {ReadAddr, 0x0};
	uint8_t Rx[2] = {0x0, 0x0};

	__MEMS_CS_LOW;
	HAL_SPI_TransmitReceive(handle, (uint8_t*)Tx, (uint8_t*)Rx, 2, 1000);
	__MEMS_CS_HIGH;

	*pBuffer = Rx[1];

	return 0;
}

/*******************************************************************************
* Function Name		: LIS3DH_WriteReg
* Description		: Generic Writing function. It must be fullfilled with either
*			: I2C or SPI writing function
* Input			: Register Address, Data to be written
* Output		: None
* Return		: None
*******************************************************************************/
uint8_t MEMS_Write_Reg(void *handle, uint8_t WriteAddr, uint8_t *pBuffer, uint16_t nBytesToWrite){ // u8_t WriteAddr, u8_t Data) {

	uint8_t Tx[2] = {WriteAddr, *pBuffer};

	__MEMS_CS_LOW;
	HAL_SPI_Transmit(handle, (uint8_t*)Tx, 2, 1000);
	__MEMS_CS_HIGH;

	return 0;
}

